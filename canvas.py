#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 12:33:29 2020

@author: robert
"""

import matplotlib.pyplot as plt
from PIL import Image

import numpy as np
import pygame

bar_im = Image.open("resources/bar.png")
background_im = Image.new('RGB', (0,0))
final_im = Image.new('RGB', (0,0))
data = []
x_bar_spacing_coef = 0
x_bar_spacing = 0

pygame.init()
screen = pygame.display.set_mode((0, 0))

class Canvas:
    bar_im = Image.open("resources/bar.png")
    background_im = Image.new('RGB', (0,0))
    final_im = Image.new('RGB', (0,0))
    data = []
    x_bar_spacing_coef = 0
    x_bar_spacing = 0

    def __init__(self, size, n_bars):
        x_size_unit = (int) (size[0] / (n_bars + self.x_bar_spacing_coef * (n_bars - 1)))
        x_size = x_size_unit * (n_bars + self.x_bar_spacing_coef * (n_bars - 1))
        
        self.background_im = Image.new('RGB', (size[1], x_size))
        bar_size = (size[1], x_size_unit)
        self.bar_im = self.bar_im.rotate(180)
        self.bar_im = self.bar_im.resize(bar_size)    
        self.x_bar_spacing = (int) (self.x_bar_spacing_coef * x_size_unit)
        
        self.screen = pygame.display.set_mode(size)
        
        self.data = [0] * n_bars
        
    def drawBar(self, perc, n):
        if (perc > 1):
            perc = 1
        elif (perc < 0):
            perc = 0
        xy = (0, 0, (int) (self.bar_im.size[0] * perc), self.bar_im.size[1])
        xy = ((int) (self.bar_im.size[0] * (1 - perc)), 0, self.bar_im.size[0], self.bar_im.size[1])
        buffer_im = self.bar_im.crop(xy)
        x_offset = ((self.bar_im.size[1] + self.x_bar_spacing) * n)
        self.final_im.paste(buffer_im, ((int) (self.bar_im.size[0] * (1 - perc)), x_offset))
        
    def visualize(self, data):
        self.final_im = self.background_im.copy()
        for i in range(0, len(data)):
            self.drawBar(data[i], i)
        surface = pygame.surfarray.make_surface(np.array(self.final_im.rotate(0)))
        screen.blit(surface, (0, 0))
        pygame.display.flip()
