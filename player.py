#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 19:54:15 2020

@author: robert
"""

import pyaudio
import wave
import sys
import signal
 
import pyaudio
import wave
import sys

from visualizer import Visualizer

class AudioFile:
    chunk = 1024

    def __init__(self, file):
        """ Init audio stream """ 
        self.wf = wave.open(file, 'rb')
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(
            format = self.p.get_format_from_width(self.wf.getsampwidth()),
            channels = self.wf.getnchannels(),
            rate = self.wf.getframerate(),
            output = True
        )
        self.visualizer = Visualizer((600, 300), 32, 44100, self.chunk, 'A')

    def play(self):
        """ Play entire file """
        data = self.wf.readframes(self.chunk)
        while data != '':
            self.stream.write(data)
            self.visualizer.visualize(data)
            data = self.wf.readframes(self.chunk)

    def close(self):
        """ Graceful shutdown """ 
        self.stream.close()
        self.p.terminate()
        
#a = AudioFile("resources/forsaken.wav")
#a.play()
#a.close()
a = AudioFile("resources/audio1.wav")
a.play()
a.close()
        
#a = AudioFile("resources/100hz.wav")
#a.play()
#a.close()
#        
#b = AudioFile("resources/250hz.wav")
#b.play()
#b.close()
#        
#a = AudioFile("resources/440hz.wav")
#a.play()
#a.close()
#        
a = AudioFile("resources/1khz.wav")
a.play()
a.close()
        
a = AudioFile("resources/10khz.wav")
a.play()
a.close()

def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    
    # close PyAudio (5)
    
    a.close()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

