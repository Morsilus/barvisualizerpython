#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 20:16:02 2020

@author: robert
"""

from scipy.fft import fft
import math
import numpy as np

from canvas import Canvas

class Visualizer:
    def __init__(self, size, n_bars, sampling_rate, chunk_size, weighting):
        self.canvas = Canvas(size, n_bars)
        self.n_bars = n_bars
        self.sampling_rate = sampling_rate
        self.chunk_size = chunk_size
        self.freqs = self.calcFreqs()
        self.weightings = self.calcWeighting(weighting, 0.75, self.freqs)
        self.max_value = 0;
        self.previous = [0] * self.n_bars
        self.alpha = 0.65
        
    def visualize(self, input_stream):
        data = np.fromstring(input_stream,dtype='<i2')
        data = self.addHamming(data)
        samples = self.calcDBs(data, -3)
        samples = self.addWeighting(samples)
#        samples = self.getPowerRatio(samples)
        samples = self.getAmplitudeRatio(samples)
        for i in range(0, len(self.previous)):
            if (samples[i] < self.previous[i]):
                samples[i] = self.alpha * self.previous[i] + (1 - self.alpha) * samples[i]
        self.previous = samples
#        samples = self.getAmplitudeRatio(samples)
#        samples2 = self.getDBofFreqs(samples, 800, 10000)
#        samples = self.getDBofFreqs(samples, 0, 16000)
#        samples = self.melScale(samples)
#        samples = self.logScale(samples)
        samples = self.pitchScale(samples)
#        samples = self.octaveScale(samples, 2)
#        samples2 = self.divide(samples2)
#        for i in range(0, len(samples)):
#            samples[i] = np.max([samples[i], samples2[i]])
#        print(samples)
        self.canvas.visualize(samples)
        
    def calcDBs(self, data, db_offset):        
        amplitudes = fft(data)[0 : (len(data) // 4)]
        dbs = 10 * np.log10(np.abs(amplitudes)) - 60 + db_offset
        return dbs
        
    def addHamming(self, data):
        return data * np.hanning(len(data))
    
    def addWeighting(self, samples):
        return samples + self.weightings
    
    def getPowerRatio(self, samples):
        return pow(10, samples / 10)
    
    def getAmplitudeRatio(self, samples):
        return pow(10, samples / 20)
    
    def divide(self, samples):
        return self.linDivision(samples, self.n_bars)
    
    def calcFreqs(self):
        n = (int) (self.chunk_size / 2)
#        n = self.chunk_size
        freqs = [0] * n
        for i in range(0, n):
            freqs[i] = ((self.sampling_rate / 2) / (n)) * (i + 1)
        return freqs
        
    def calcWeighting(self, weighting, ratio, freqs):
        n = (int) (self.chunk_size / 2)
#        n = self.chunk_size
        weightings = [0] * n
        if (weighting == 'A'):
            for i in range(0, n):
                weightings[i] = self.weightingA(freqs[i]) * ratio
        elif (weighting == 'B'):
            for i in range(0, n):
                weightings[i] = self.weightingB(freqs[i]) * ratio
        return weightings
    
    def weightingA(self, freq):
        freq2 = pow(freq, 2)        
        return 20 * math.log10((pow(12194, 2) * pow(freq, 4)) /
                               ((freq2 + pow(20.6, 2)) * (freq2 + pow(12194, 2)) *
                                math.sqrt((freq2 + pow(107.7, 2)) * (freq2 + pow(737.9, 2))))) + 2
    
    def weightingB(self, freq):
        freq2 = pow(freq, 2)        
        return 20 * math.log10((pow(12194, 2) * pow(freq, 3)) /
                               ((freq2 + pow(20.6, 2)) * (freq2 + pow(12194, 2)) *
                                math.sqrt(freq2 + pow(158.5, 2)))) + 0.17
                               
    def linScale(self, samples):
        divided = [0] * self.n_bars
        unit = (len(samples) / self.n_bars)
        for i in range(0, self.n_bars):
            lower = (int) (i * unit)
            upper = (int) ((i + 1) * unit)
            divided[i] = np.max(samples[lower : upper])
        return divided
    
    def logScale(self, samples):
        divided = [0] * self.n_bars
        for i in range(0, self.n_bars):
            lower = 20 * pow(22050 / 20, i / (self.n_bars))
            upper = 20 * pow(22050 / 20, (i + 1) / (self.n_bars))
            buffer = self.getDBofFreqs(samples, lower, upper)
            if (len(buffer) > 0):
                divided[i] = np.max(buffer)
        return divided
    
    def melScale(self, samples):
        divided = [0] * self.n_bars
        unit = self.hertzToMel(8000) / self.n_bars
        for i in range(0, self.n_bars):
            lower_f = self.melToHertz(i * unit)
            upper_f = self.melToHertz((i + 1) * unit)
            divided[i] = np.max(self.getDBofFreqs(samples, lower_f, upper_f))
        return divided
        
    def hertzToMel(self, freq):
#        return 2595 * math.log10(1 + freq / 700)
        return 2410 * math.log10(1 + freq / 625)
    
    def melToHertz(self, freq):
        return 700 * (pow(10, freq / 2595) - 1)
    
    def pitchScale(self, samples):
        divided = [0] * self.n_bars
        min_p = self.hertzToPitch(150) # 150
        max_p = self.hertzToPitch(8000) # 8000
        unit = (max_p - min_p) / self.n_bars
#        print(unit)
        for i in range(0, self.n_bars):
            lower_f = self.pitchToHertz(i * unit + min_p)
            upper_f = self.pitchToHertz((i + 1) * unit + min_p)
#            print(lower_f)
#            print(upper_f)
            buffer = self.getDBofFreqs(samples, lower_f, upper_f)
            divided[i] = buffer
#            if (len(buffer) > 0):
#                divided[i] = np.max(buffer)
        return divided
    
    def hertzToPitch(self, freq):
        return 12 * math.log2(freq / 440) + 69
    
    def pitchToHertz(self, freq):
        return 440 * (math.pow(2, (freq - 69) / 12))
    
    def octaveScale(self, samples, octave):
        n = 8 * octave
        divided = [0] * n
        for i in range(0, n):
            lower = 15.625 * pow(2, (i + 0) / octave)
            upper = 15.625 * pow(2, (i + 1) / octave)
            buffer = self.getDBofFreqs(samples, lower, upper)
            if (len(buffer) > 1):                
                divided[i] = np.max(buffer)
            elif (len(buffer) == 1):
                divided[i] = buffer[0]
        return divided
            
    def getDBofFreqs(self, samples, lower_f, upper_f):
        freq_unit = (self.sampling_rate / 2) / (self.chunk_size / 2)#/ 2 + 1)
        lower_i = (int) (lower_f / freq_unit)
        upper_i = (int) (upper_f / freq_unit)
        return samples[(upper_i - lower_i) // 2 + lower_i]
        
